# -*- coding: utf-8 -*-

"""Define a class.

    The following documentation is automatically generated from the Python
    source files.  It may be incomplete, incorrect or include features that
    are considered implementation detail and may vary between Python
    implementations.  When in doubt, consult the module reference at the
    location listed above."""


class SAspi:
    def __init__(self, n_x=1, n_y=1, n_d=0, vertices=None, edges=None,
                 robots=None):
        self.n_x = n_x
        self.n_y = n_y
        self.n_d = n_d
        self.vertices = set(vertices or set(range(n_x * n_y)))
        self.edges = set(edges or set())
        self.robots = dict(robots or dict())

    def __repr__(self):
        return 'SAspi(n_x=%d, n_y=%d, n_d=%d, vertices=%s, edges=%s,' \
               ' robots=%s)' % (self.n_x, self.n_y, self.n_d, self.vertices,
                                self.edges, self.robots)

    def read(self, file_name):
        f = open(file_name, 'r')
        lines = f.read().strip().split('\n')
        line = lines[0].split(' ')
        # Read three constants.
        n_x = int(line[0])
        n_y = int(line[1])
        n_d = int(line[2])
        # length = n_x * n_y
        edges = set()
        bad_edges = set()
        vertices = set(range(n_x * n_y))
        # Up, Right, Down, Left.
        directions = [(-1, -n_y), (1, 1), (1, n_y), (-1, -1)]
        # Setting edges.
        for i, line in enumerate(lines[1:n_x + 1]):
            wall = int(line, 0x10)
            for k in range(n_y):
                j = n_y - 1 - k
                pos = i * n_y + j
                for l, d in enumerate(directions):
                    # Even for verticale direction otherwise horizontale.
                    if l % 2 == 0:
                        i_ = i + d[0]
                        j_ = j
                    else:
                        i_ = i
                        j_ = j + d[0]
                    if i_ < 0 or i_ >= n_x or j_ < 0 or j_ >= n_y:
                        # TODO: extra tests to check if the boundary is closed.
                        continue
                    # pos_ = i_ * n_y + j_
                    pos_ = pos + d[1]
                    if wall & (1 << (l + 4 * k)) == 0:
                        # sys.getsizeof(frozenset((1, 2))) = 216
                        # sys.getsizeof((1, 2)) = 56
                        # edges.add(frozenset((pos, pos_)))
                        edges.add((pos, pos_))
                        edges.add((pos_, pos))
                    else:
                        bad_edges.add((pos, pos_))
                        bad_edges.add((pos_, pos))
        # TODO: Report this?
        edges.difference_update(bad_edges)  # Due to malformation.
        robots = dict()
        # Robot's position.
        for line in lines[1 + n_x:]:
            s = line.split(' ')
            robots[s[0]] = n_y * int(s[1]) + int(s[2])
        self.edges = edges
        self.vertices = vertices
        self.robots = robots
        self.n_x = n_x
        self.n_y = n_y
        self.n_d = n_d
        f.close()
        # print(vertices)
        # print(robots)
        # print(len(edges), edges)

    def find_optimal_paths0(self, full=True):
        # Better? Not clear.
        # TODO: Prove it find a best path.
        states = list()
        # 1st

        def ones(n):
            count = 0
            while n > 0:
                # count += (n & 1)
                # n >>= 1
                n &= n - 1
                count += 1
            return count

        # Robot's position initialization.
        robots = 0
        log2_pos = max(len(self.vertices) - 1, 1).bit_length()
        robot_num = len(self.robots)
        for i, (_, v) in enumerate(self.robots.items()):
            robots |= v << (i * log2_pos)
        # Path initialization.
        path = 0
        # Vertices initialization.
        vertices = 0
        for v in self.robots.values():
            vertices |= (1 << v)
        robots_pos = list((robots >> (log2_pos * i)) & (2 ** log2_pos - 1)
                          for i in range(robot_num))
        robots_ = sum(pos << (i * log2_pos)
                      for i, pos in enumerate(sorted(robots_pos)))
        states.append((robots_, path, vertices))
        # Constants & others initialization.
        all_vertices = 2 ** len(self.vertices) - 1
        visited = [set() for _ in range(len(self.vertices) + 1)]
        n_actions = [('N', -self.n_y), ('E', 1), ('S', self.n_y), ('W', -1)]
        name_dir = max(robot_num - 1, 1).bit_length() + 2
        paths = list()
        path_len = 0
        while len(states) > 0:
            path_len += 1
            next_states = list()
            # print(len(states), sum(len(v) for v in visited),
            #       [len(v) for v in visited])
            # print(states)
            if full:
                for state in states:
                    robots_pos = [(state[0] >> (log2_pos * i))
                                  & (2 ** log2_pos - 1)
                                  for i in range(robot_num)]
                    robots_ = sum(pos << (i * log2_pos)
                                  for i, pos in enumerate(sorted(robots_pos)))
                    visited[ones(state[2])].add((robots_, state[2]))
            while len(states) > 0:
                state = states.pop()
                robots_pos = [(state[0] >> (log2_pos * i))
                              & (2 ** log2_pos - 1) for i in range(robot_num)]
                if not full:
                    robots_ = sum(pos << (i * log2_pos)
                                  for i, pos in enumerate(sorted(robots_pos)))
                    if (robots_, state[2]) not in visited[ones(state[2])]:
                        visited[ones(state[2])].add((robots_, state[2]))
                    else:
                        continue
                # For each robot.
                for i, pos in enumerate(robots_pos):
                    # next_robots = [p for p in robots if p != pos]
                    next_robots_pos = list(robots_pos)
                    next_robots_pos.remove(pos)
                    # For each direction.
                    for n, action in enumerate(n_actions):
                        next_vertices = 0
                        next_pos = pos
                        # Move the robot.
                        while True:
                            new_pos = next_pos + action[1]
                            if new_pos not in next_robots_pos \
                                    and (new_pos, next_pos) in self.edges:
                                next_pos = new_pos
                            else:
                                break
                            next_vertices |= (1 << next_pos)
                        if pos == next_pos:
                            continue
                        next_robots = state[0]
                        next_robots ^= pos << (log2_pos * i)  # Set to zero.
                        next_robots |= next_pos << (log2_pos * i)
                        next_vertices |= state[2]
                        next_path = state[1] << name_dir
                        next_path += (i << 2) + n
                        next_robots_pos.append(next_pos)
                        next_robots_ = sum(pos_ << (j * log2_pos)
                                           for j, pos_ in
                                           enumerate(sorted(next_robots_pos)))
                        next_robots_pos.remove(next_pos)
                        # Check if already visited.
                        if (next_robots_, next_vertices) \
                                in visited[ones(next_vertices)]:
                            continue
                        next_states.append((next_robots, next_path,
                                            next_vertices))
            # print(next_states)
            paths = [path for robots, path, vertices in next_states
                     if vertices == all_vertices]
            if len(paths) > 0:
                break
            states = next_states
        # Decompression.
        names = [k for k, _ in self.robots.items()]
        next_paths = list()
        for path in paths:
            p = [(path >> (name_dir * i)) & (2 ** name_dir - 1)
                 for i in range(path_len)]
            next_path = [names[q >> 2] + n_actions[q & 0x03][0]
                         for q in reversed(p)]
            next_paths.append(next_path)
        paths = next_paths
        return paths

    def find_optimal_paths(self, full=True):
        # This solution has some default, it uses too much memory
        # and 'starts' to slow down.
        # TODO: Prove that this algorithm finds some best solutions.
        states = list()
        # ones = lambda n: sum([(n >> i) & 1 for i in range(n.bit_length())])
        # 1st

        def ones(n):
            count = 0
            while n > 0:
                # count += (n & 1)
                # n >>= 1
                n &= n - 1
                count += 1
            return count

        # Robot's position initialization.
        robots = 0
        log2_pos = max(len(self.vertices) - 1, 1).bit_length()
        robot_num = len(self.robots)
        for i, (_, v) in enumerate(self.robots.items()):
            robots |= v << (i * log2_pos)
        # Path initialization.
        path = 0
        # Vertices initialization.
        vertices = 0
        for v in self.robots.values():
            vertices |= (1 << v)
        states.append((robots, path, vertices))
        # Constants & others initialization.
        all_vertices = 2 ** len(self.vertices) - 1
        visited = [set() for _ in range(len(self.vertices) + 1)]
        n_actions = [('N', -self.n_y), ('E', 1), ('S', self.n_y), ('W', -1)]
        name_dir = max(robot_num - 1, 1).bit_length() + 2
        paths = list()
        path_len = 0
        while len(states) > 0:
            path_len += 1
            next_states = list()
            # print(len(states), sum(len(v) for v in visited),
            #       [len(v) for v in visited])
            # print(states)
            if full:
                for state in states:
                    visited[ones(state[2])].add((state[0], state[2]))
            while len(states) > 0:
                state = states.pop()
                robots_pos = [(state[0] >> (log2_pos * i))
                              & (2 ** log2_pos - 1) for i in range(robot_num)]
                if not full:
                    if (state[0], state[2]) not in visited[ones(state[2])]:
                        visited[ones(state[2])].add((state[0], state[2]))
                    else:
                        continue
                # For each robot.
                for i, pos in enumerate(robots_pos):
                    # next_robots = [p for p in robots if p != pos]
                    next_robots_pos = list(robots_pos)
                    next_robots_pos.remove(pos)
                    # For each direction.
                    for n, action in enumerate(n_actions):
                        next_vertices = 0
                        next_pos = pos
                        # Move the robot.
                        while True:
                            new_pos = next_pos + action[1]
                            if new_pos not in next_robots_pos \
                                    and (new_pos, next_pos) in self.edges:
                                next_pos = new_pos
                            else:
                                break
                            next_vertices |= (1 << next_pos)
                        if pos == next_pos:
                            continue
                        next_robots = state[0]
                        next_robots ^= pos << (log2_pos * i)  # Set to zero.
                        next_robots |= next_pos << (log2_pos * i)
                        next_vertices |= state[2]
                        next_path = state[1] << name_dir
                        next_path += (i << 2) + n
                        # Check if already visited.
                        if (next_robots, next_vertices) \
                                in visited[ones(next_vertices)]:
                            continue
                        next_states.append((next_robots, next_path,
                                            next_vertices))
            # print(next_states)
            paths = [path for robots, path, vertices in next_states
                     if vertices == all_vertices]
            if len(paths) > 0:
                break
            states = next_states
        # Decompression.
        names = [k for k, _ in self.robots.items()]
        next_paths = list()
        for path in paths:
            p = [(path >> (name_dir * i)) & (2 ** name_dir - 1)
                 for i in range(path_len)]
            next_path = [names[q >> 2] + n_actions[q & 0x03][0]
                         for q in reversed(p)]
            next_paths.append(next_path)
        # next_paths = [[names[q >> 2] + n_actions[q & 0x03][0]
        #               for q in reversed(\
        #                 (path >> (name_dir * i)) & (2 ** name_dir - 1) \
        #                 for i in range(path_len))] \
        #               for path in paths]
        paths = next_paths
        return paths

    def find_optimal_paths2(self, robots=None, path=None, vertices=None,
                            visited=None, properties=None, min_path=None):
        # Slow and until 2 only
        init = False
        ones = lambda n: sum((n >> i) & 1 for i in range(n.bit_length()))
        if robots is None:
            init = True
            # Robot's position initialization.
            robots = 0
            log2_pos = max(len(self.vertices) - 1, 1).bit_length()
            robot_num = len(self.robots)
            for i, (_, v) in enumerate(self.robots.items()):
                robots |= v << (i * log2_pos)
            # Path initialization.
            path = (0, 0)
            # Vertices initialization.
            vertices = 0
            for v in self.robots.values():
                vertices |= (1 << v)
            # Constants & others initialization.
            all_vertices = 2 ** len(self.vertices) - 1
            visited = [set() for _ in range(len(self.vertices) + 1)]
            n_actions = [('N', -self.n_y), ('E', 1), ('S', self.n_y),
                         ('W', -1)]
            name_dir = max(len(self.robots) - 1, 1).bit_length() + 2
            properties = (log2_pos, robot_num, all_vertices, n_actions,
                          name_dir)
            min_path = [len(self.vertices)]
        if min_path[0] is not None and path[1] > min_path[0]:
            return list()
        if vertices == properties[2]:
            if min_path[0] is None or min_path[0] > path[1]:
                min_path[0] = path[1]
                print(path[1])
            return [path]
        visited[ones(vertices)].add((robots, vertices))
        robots_pos = [(robots >> (properties[0] * i))
                      & (2 ** properties[0] - 1) for i in range(properties[1])]
        # print(robots, path, '{:b}'.format(vertices), visited)
        paths = list()
        # For each robot.
        for i, pos in enumerate(robots_pos):
            next_robots_pos = list(robots_pos)
            next_robots_pos.remove(pos)
            # For each direction.
            for n, action in enumerate(properties[3]):
                next_vertices = 0
                next_pos = pos
                # Move the robot.
                while True:
                    new_pos = next_pos + action[1]
                    if new_pos not in next_robots_pos \
                            and (new_pos, next_pos) in self.edges:
                        next_pos = new_pos
                    else:
                        break
                    next_vertices |= (1 << next_pos)
                if next_pos == pos:
                    continue
                next_robots = robots
                next_robots ^= pos << (properties[0] * i)  # Set to zero.
                next_robots |= next_pos << (properties[0] * i)
                next_vertices |= vertices
                next_path = ((path[0] << properties[4]) + (i << 2) + n,
                             path[1] + 1)
                # Check if already visited.
                if (next_robots, next_vertices) \
                        in visited[ones(next_vertices)]:
                    continue
                paths.extend(self.find_optimal_paths2(next_robots, next_path,
                                                      next_vertices, visited,
                                                      properties, min_path))
        visited[ones(vertices)].remove((robots, vertices))
        if init:
            m = min([path[1] for path in paths])
            paths = [path for path in paths if path[1] == m]
            # Decompression.
            names = [k for k, _ in self.robots.items()]
            next_paths = list()
            for path in paths:
                p = [(path >> (name_dir * i)) & (2 ** name_dir - 1)
                     for i in range(path[1])]
                next_path = [names[q >> 2] + n_actions[q & 0x03][0]
                             for q in reversed(p)]
                next_paths.append(next_path)
            paths = next_path
        return paths

    def find_optimal_paths3(self):
        # Slow and not optimal for 3
        states = list()
        # 3rd
        # ones = lambda n: sum([(n >> i) & 1 for i in range(n.bit_length())])
        # 1st

        def ones(n):
            count = 0
            while n > 0:
                # count += (n & 1)
                # n >>= 1
                n &= n - 1
                count += 1
            return count

        # ones = lambda n: bin(n).count('1')  # 2nd
        # Robot's position initialization.
        robots = 0
        log2_pos = max(len(self.vertices) - 1, 1).bit_length()
        robot_num = len(self.robots)
        for i, (_, v) in enumerate(self.robots.items()):
            robots |= v << (i * log2_pos)
        # Path initialization.
        path = (0, 0)
        # Vertices initialization.
        vertices = 0
        for v in self.robots.values():
            vertices |= (1 << v)
        states.append((robots, path, vertices))
        # Constants & others initialization.
        all_vertices = 2 ** len(self.vertices) - 1
        # visited = list()
        visited = [set() for _ in range(len(self.vertices) + 1)]
        # visited.append(set())
        n_actions = [('N', -self.n_y), ('E', 1), ('S', self.n_y), ('W', -1)]
        name_dir = max(robot_num - 1, 1).bit_length() + 2
        # paths = list()
        paths = set()
        min_path = None
        # max_path = None
        pre_ones = 0
        max_ones = None
        # len_vertices = len(self.vertices)
        while len(states) > 0:
            # next_states = list()
            # print([p[1] for _, p, _ in states])
            # if max_ones is None:
            #     max_ones = max(ones(v) for _, _, v in states)
            max_ones = max(ones(v) for _, _, v in states)
            if pre_ones >= max_ones:
                for i in range(max_ones + 1, len(visited)):
                    visited[i] = set()
            pre_ones = max_ones
            low_states = list()
            max_states = list()
            for state in states:
                if ones(state[2]) == max_ones:
                    max_states.append(state)
                else:
                    low_states.append(state)
            print(len(states), len(max_states))
            states = low_states
            # next_max_ones = 0
            while len(max_states) > 0:
                state = max_states.pop()
                visited[max_ones + 1].add((state[0], state[2]))
                robots_pos = [(state[0] >> (log2_pos * i))
                              & (2 ** log2_pos - 1) for i in range(robot_num)]
                # For each robot.
                for i, pos in enumerate(robots_pos):
                    next_robots_pos = list(robots_pos)
                    next_robots_pos.remove(pos)
                    # For each direction.
                    for n, action in enumerate(n_actions):
                        next_vertices = 0
                        next_pos = pos
                        # Move the robot.
                        while True:
                            new_pos = next_pos + action[1]
                            if new_pos not in next_robots_pos \
                                    and (new_pos, next_pos) in self.edges:
                                next_pos = new_pos
                            else:
                                break
                            next_vertices |= (1 << next_pos)
                        if pos == next_pos:
                            continue
                        next_robots = state[0]
                        next_robots ^= pos << (log2_pos * i)  # Set to zero.
                        next_robots |= next_pos << (log2_pos * i)
                        next_vertices |= state[2]
                        next_path = ((state[1][0] << name_dir) + (i << 2) + n,
                                     state[1][1] + 1)
                        # Check if already visited.
                        if (next_robots, next_vertices) in visited[max_ones]:
                            continue
                        # next_states.append((next_robots, next_path,
                        #                     next_vertices))
                        # next_max_ones =
                        # max(next_max_ones, ones(next_vertices))
                        if next_vertices == all_vertices:
                            paths.add(next_path)
                            if min_path is None or min_path > next_path[1]:
                                min_path = next_path[1]
                            continue
                        states.append((next_robots, next_path, next_vertices))
            visited[max_ones].update(visited[max_ones + 1])
            visited[max_ones + 1] = set()
            # if max_ones <= next_max_ones < len_vertices:
            #     max_ones = next_max_ones
            # else:
            #     max_ones = None
            # paths.update(path for robots, path, vertices in next_states
            #              if vertices == all_vertices)
            # for _, path, vertices in states:
            #     if vertices == all_vertices:
            #         paths.add(path)
            #         if min_path is None or min_path < path[1]:
            #             min_path = path[1]
            # paths.update(path for robots, path, vertices in states
            #              if vertices == all_vertices)
            # if len(paths) > 0:
            #     min_path = min(paths, key=lambda x: x[1])[1]
            # states.extend(next_states)  # quite expensive.
            if min_path is not None:
                states = list(filter(lambda x: x[1][1] < min_path, states))
            # states = list(filter(lambda x: x[2] != all_vertices, states))
            print('Paths:', len(paths), set(x[1] for x in paths),
                  set(x[1][1] for x in states))
        # print('Min path:', min_path)
        # Decompression.
        names = [k for k, _ in self.robots.items()]
        next_paths = list()
        for path in paths:
            p = [(path >> (name_dir * i)) & (2 ** name_dir - 1)
                 for i in range(path[1])]
            next_path = [names[q >> 2] + n_actions[q & 0x03][0]
                         for q in reversed(p)]
            next_paths.append(next_path)
        paths = next_path
        # return paths
        return list(filter(lambda x: x[1] == min_path, paths))

    def find_optimal_paths4(self):
        # Decent but doesn't work on 3.
        # import math
        states = list()
        # 3rd
        # ones = lambda n: sum([(n >> i) & 1 for i in range(n.bit_length())])
        # 1st

        def ones(n):
            count = 0
            while n > 0:
                # count += (n & 1)
                # n >>= 1
                n &= n - 1
                count += 1
            return count

        # ones = lambda n: bin(n).count('1')  # 2nd
        # Robot's position initialization.
        robots = 0
        log2_pos = max(len(self.vertices) - 1, 1).bit_length()
        robot_num = len(self.robots)
        for i, (_, v) in enumerate(self.robots.items()):
            robots |= v << (i * log2_pos)
        # Path initialization.
        path = (0, 0)
        # Vertices initialization.
        vertices = 0
        for v in self.robots.values():
            vertices |= (1 << v)
        states.append((robots, path, vertices, 0))
        # Constants & others initialization.
        all_vertices = 2 ** len(self.vertices) - 1
        # visited = [set() for _ in range(len(self.vertices) + 1)]
        visited = list()
        visited.append([set() for _ in range(len(self.vertices) + 1)])
        # visited.append(set())
        n_actions = [('N', -self.n_y), ('E', 1), ('S', self.n_y), ('W', -1)]
        name_dir = max(robot_num - 1, 1).bit_length() + 2
        # states.append((robots, path, vertices,
        #                len(self.vertices) - ones(vertices)))
        # paths = list()
        paths = set()
        min_path = None
        # max_path = None
        # pre_ones = 0
        # max_ones = None
        len_vertices = len(self.vertices)
        while len(states) > 0:
            # next_states = list()
            # print([p[1] for _, p, _ in states])
            # if max_ones is None:
            #     max_ones = max(ones(v) for _, _, v in states)
            # max_ones = max(ones(v) for _, _, v, _ in states)
            # if pre_ones >= max_ones:
            #     for i in range(max_ones + 1, len(visited)):
            #         visited[i] = set()
            # pre_ones = max_ones
            hi_states = list()
            lo_states = list()
            min_score = min(s for _, _, _, s in states)
            for state in states:
                if state[3] <= min_score:
                    lo_states.append(state)
                else:
                    hi_states.append(state)
            # Not good.
            # min_score = max((len_vertices - s) / (p[1] + 1)
            #                 for _, p, _, s in states)
            # min_score = math.floor(min_score * 10) / 10
            # for state in states:
            #     if (len_vertices - state[3]) / (state[1][1] + 1) >= min_score:
            #         lo_states.append(state)
            #     else:
            #         hi_states.append(state)
            print(len(states), len(lo_states), min_score)
            print('Paths:', len(paths), set(x[1] for x in paths),
                  set(x[1][1] for x in states))
            # for state in states:
            #     visited[state[1][1]][ones(state[2])].add((state[0], state[2]))
            states = hi_states
            # next_max_ones = 0
            while len(lo_states) > 0:
                state = lo_states.pop()
                while len(visited) <= state[1][1]:
                    visited.append([set() for _ in
                                    range(len(self.vertices) + 1)])
                visited[state[1][1]][ones(state[2])].add((state[0], state[2]))
                robots_pos = [(state[0] >> (log2_pos * i))
                              & (2 ** log2_pos - 1) for i in range(robot_num)]
                # For each robot.
                for i, pos in enumerate(robots_pos):
                    next_robots_pos = list(robots_pos)
                    next_robots_pos.remove(pos)
                    # For each direction.
                    for n, action in enumerate(n_actions):
                        next_vertices = 0
                        next_pos = pos
                        # Move the robot.
                        while True:
                            new_pos = next_pos + action[1]
                            if new_pos not in next_robots_pos \
                                    and (new_pos, next_pos) in self.edges:
                                next_pos = new_pos
                            else:
                                break
                            next_vertices |= (1 << next_pos)
                        if pos == next_pos:
                            continue
                        next_robots = state[0]
                        next_robots ^= pos << (log2_pos * i)  # Set to zero.
                        next_robots |= next_pos << (log2_pos * i)
                        # next_score = state[3] \
                        #     + ones(next_vertices & state[2]) \
                        #     - (ones(next_vertices)
                        #         - ones(next_vertices & state[2]))
                        # Bad score?
                        next_score = state[3] + ones(next_vertices & state[2])
                        # print('score:', state[3], next_score,
                        # '{:b} {:b} {:b}'.format(state[2], next_vertices,
                        # next_vertices | state[2]))
                        next_vertices |= state[2]
                        next_path = ((state[1][0] << name_dir) + (i << 2) + n,
                                     state[1][1] + 1)
                        # Check if already visited.
                        unique = True
                        for k in range(next_path[1]):
                            if (next_robots, next_vertices) \
                                    in visited[k][ones(next_vertices)]:
                                unique = False
                                break
                        if not unique:
                            continue
                        # next_states.append((next_robots, next_path,
                        #                     next_vertices))
                        # next_max_ones =
                        # max(next_max_ones, ones(next_vertices))
                        if next_vertices == all_vertices:
                            paths.add(next_path)
                            if min_path is None or min_path > next_path[1]:
                                min_path = next_path[1]
                            continue
                        states.append((next_robots, next_path, next_vertices,
                                       next_score))
            # visited[max_ones].update(visited[max_ones + 1])
            # visited[max_ones + 1] = set()
            # if max_ones <= next_max_ones < len_vertices:
            #     max_ones = next_max_ones
            # else:
            #     max_ones = None
            # paths.update(path for robots, path, vertices in next_states
            #              if vertices == all_vertices)
            # for _, path, vertices in states:
            #     if vertices == all_vertices:
            #         paths.add(path)
            #         if min_path is None or min_path < path[1]:
            #             min_path = path[1]
            # paths.update(path for robots, path, vertices in states
            #              if vertices == all_vertices)
            # if len(paths) > 0:
            #     min_path = min(paths, key=lambda x: x[1])[1]
            # states.extend(next_states)  # quite expensive.
            if min_path is not None:
                states = list(filter(lambda x: x[1][1] < min_path - 1, states))
            # states = list(filter(lambda x: x[2] != all_vertices, states))
        print('Min path:', min_path)
        # Decompression.
        names = [k for k, _ in self.robots.items()]
        next_paths = list()
        for path in paths:
            p = [(path >> (name_dir * i)) & (2 ** name_dir - 1)
                 for i in range(path[1])]
            next_path = [names[q >> 2] + n_actions[q & 0x03][0]
                         for q in reversed(p)]
            next_paths.append(next_path)
        paths = next_path
        # return paths
        return list(filter(lambda x: x[1] == min_path, paths))


if __name__ == '__main__':
    import time
    import datetime
    a = SAspi()
    print(a)
    # a.read('Case_Aspi_R_1.txt')
    # s = a.find_optimal_paths0(full=True)
    # s0 = a.find_optimal_paths0(full=False)
    # s = [' '.join(t) for t in s]
    # s0 = [' '.join(t) for t in s0]
    # print(s, s0)
    # ss = set(s)
    # ss0 = set(s0)
    # if len(ss) < len(ss0):
    #     t = ss0.difference(ss)
    # else:
    #     t = ss.difference(ss0)
    # print(len(t), t)
    for i in range(4):
        a.read('Case_Aspi_R_' + str(i) + '.txt')
        print(a)
        s = time.time()
        paths = a.find_optimal_paths0(full=False)
        e = time.time()
        # paths = set([' '.join(path) for path in paths])
        print(len(paths), paths)
        print('dt:', datetime.timedelta(seconds=e - s))

    # a.read('Case_Aspi_R_3.txt')
    import matplotlib.pyplot as plt
    # TODO: Find an alternatives?
    # fig, ax = plt.figure()
    # ax.set_aspect('equal')
    plt.axes().set_aspect('equal')
    plt.plot((0, a.n_y), (0, 0), c='k')
    plt.plot((a.n_y, a.n_y), (0, -a.n_x), c='k')
    plt.plot((0, a.n_y), (-a.n_x, -a.n_x), c='k')
    plt.plot((0, 0), (0, -a.n_x), c='k')
    for i in range(a.n_x - 1):
        for j in range(a.n_y):
            u = (j, j + 1)
            # u = (i % a.n_y, i % a.n_y + 1)
            v = (-(1 + i), -(1 + i))
            c = None
            if (i * a.n_y + j, (i + 1) * a.n_y + j) in a.edges:
                c = 'c'
            else:
                c = 'k'
            plt.plot(u, v, c=c)
    for i in range(a.n_x):
        v = (-i, -(1 + i))
        for j in range(a.n_y - 1):
            u = (1 + j, 1 + j)
            c = None
            if (i * a.n_y + j, i * a.n_y + j + 1) in a.edges:
                c = 'c'
            else:
                c = 'k'
            plt.plot(u, v, c=c)
    plt.show()
